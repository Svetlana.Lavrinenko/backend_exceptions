import java.io.InputStreamReader;
import java.util.Scanner;

public class Task9 {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(new InputStreamReader(System.in));
        String word = null;
        while (scanner.hasNext()) {
            try {

                word = scanner.next();
                if (word.length() > 10) {
                    throw new MyException(word);
                }


            } catch (MyException myException) {
                System.out.println( myException.getMessage());
            }


        }

    }
}


